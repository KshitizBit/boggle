

# Boggle

Boggle is a word game invented by Allan Turoff[1] and originally distributed by Parker Brothers.[2] The game is played using a plastic grid of lettered dice, in which players attempt to find words in sequences of adjacent letters.dddddd

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
ruby version 2.6.5

```
```
nodejs version 14.x

```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
git clone https://KshitizBit@bitbucket.org/KshitizBit/boggle.git
cd boggle
bundle install
yarn install --check-files
rails s
```
