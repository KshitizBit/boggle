import {fetchWordsSuccess } from './actionTypes';

function fetchWords(){
    return function(dispatch){
        return fetch('/v1/words')
        .then(res=> res.json())
        .then(resJson=>{
            dispatch(fetchWordsSuccess(resJson.words));
        })
    }
}
export default fetchWords;
