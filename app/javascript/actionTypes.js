export const ADD_WORD_TO_SCOREBOARD = "ADD_W_SB";
export const STOP_GAME = "STOP_GAME";
export const SET_ERR = "SET_ERR";
export const FETCH_WORDS = "FETCH_WORDS";

export function addWordToScoreBoard(word){
    return {type: ADD_WORD_TO_SCOREBOARD, word: word, score: word.length}
}
export function stopGame(){
    return {type: STOP_GAME}
}

export function setError(error){
    return {type: SET_ERR, error: error}
}
export function fetchWordsSuccess(words){
    return {
        type: FETCH_WORDS,
        words: words
    }
}
