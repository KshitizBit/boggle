import React from "react"
import PropTypes from "prop-types"
class Result extends React.Component {
    constructor(props)
    {
        super(props);
    }
    render () {
        return (
            <React.Fragment>
                <div className="jumbotron">
                <div className="container">
                    <div className="row">
                        <div className ="col-md-3">
                            <div className="table-responsive">
                                <table className="table table-sm table-bordered">
                                    <thead className='thead-dark'>
                                        <tr>
                                            <th scope="col">Word</th>
                                            <th scope="col">Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.currentScoreTable()}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-md-3">
                            <h4> Game Results</h4>
                            <h3> Your Stats </h3>
                            {this.totalScore()}
                            {this.longestWord()}
                        </div>
                        <div className="col-md-3">
                            <button className="btn btn-primary" onClick={this.goBackToGame}>Play again</button>
                        </div>

                    </div>
                </div>
                </div>
            </React.Fragment>
        );
    }
    longestWord(){
        let word = [];
        if(this.props.scoreboard.length > 0){
            let longest =  this.props.scoreboard.reduce((max, next) => max.word.length > next.word.length ? max :next);
            word.push(<div key="longest_word"><label className="font-weight-bold">Longest Word:</label> <span>{longest.word} ({longest.score})</span></div>);
        }
        return word;
    }

    totalScore(){
        let totalScore = [];
        let currentScore = this.props.scoreboard.reduce((a,b) =>a +(b['score'] || 0),0)
        totalScore.push(<div key="total_score"><label className="font-weight-bold"> Total Points: </label> <span>{currentScore}</span></div>);
        return totalScore;
    }
    currentScoreTable(){
        let scoretable = [];
        for (var i = 0, len = this.props.scoreboard.length; i < len; i++) {
            let currentWord = this.props.scoreboard[i].word.toUpperCase();
            let score = this.props.scoreboard[i].score;
            scoretable.push( <tr key={currentWord}><td>{currentWord }</td><td>{score}</td></tr>);
        }
        return scoretable
    }
    goBackToGame(){
        window.location = '/';
    }
}

export default Result
