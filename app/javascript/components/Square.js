import React from "react"
import PropTypes from "prop-types"
class Square extends React.Component {

  constructor(props){
     super(props)
  }

  render () {
    return (
      <button className="square">
          {this.props.char}
      </button>
    );
  }
}

export default Square
