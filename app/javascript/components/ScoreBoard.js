import React from "react"
import PropTypes from "prop-types"
class ScoreBoard extends React.Component {
    constructor(props){
        super(props);
    }
    render () {
        return (
            <React.Fragment>
                {this.totalScore()}
                <div className="table-responsive">
                <table className="table table-sm table-bordered">
                    <thead className='thead-dark'>
                        <tr>
                            <th scope="col">Word</th>
                            <th scope="col">Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.currentScoreTable()}
                    </tbody>
                </table>
                </div>
            </React.Fragment>
        );
    }
    totalScore(){
        let currentScore = this.props.scoreboard.reduce((a,b) =>a +(b['score'] || 0),0)
        if(currentScore > 0)
            return <h4 className="display-5">Score : {currentScore}</h4>
    }
    currentScoreTable(){
        let scoretable = [];
        for (var i = 0, len = this.props.scoreboard.length; i < len; i++) {
            let currentWord = this.props.scoreboard[i].word.toUpperCase();
            let score = this.props.scoreboard[i].score;
            scoretable.push( <tr key={currentWord}><td>{currentWord }</td><td>{score}</td></tr>);
        }
        return scoretable
    }
    addToScore(word){
        this.currentScore.push(
        );
    }
}

export default ScoreBoard
