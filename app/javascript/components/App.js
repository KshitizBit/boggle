import React, {useEffect} from "react"
import PropTypes from "prop-types"
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import {Provider} from "react-redux"
import Game from './Game'
import Result from './Result'
import store from '../configureStore';
import fetchWords from '../fetchWords';

class App extends React.Component {
  constructor(props){
        super(props)
        store.dispatch(fetchWords())
    }
  render () {
    return (
        <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" render={()=><Game store={store}/>}/>
                <Route exact path="/results" render={()=><Result scoreboard={store.getState().scoreboard}/>}/>
            </Switch>
        </BrowserRouter>
        </Provider>
    );
  }
}

export default App
