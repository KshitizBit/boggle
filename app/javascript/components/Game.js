import React from "react";
import PropTypes from "prop-types";
import VisibleBoard from "../containers/VisibleBoard";
import VisibleScoreBoard from '../containers/VisibleScoreBoard';
import VisibleAddWord from '../containers/VisibleAddWord';
import Timer from "./Timer";
import {addWordToScoreBoard, stopGame,setError} from "../actionTypes";
import {withRouter} from 'react-router-dom';

class Game extends React.Component {
    constructor(props){
        super(props);
    }
    render () {
        return (
            <div className="jumbotron">
                <div className="container">
                    <div className ="row">
                        <div className="col-md-3">
                            <VisibleBoard x={4}>
                            </VisibleBoard>
                            <VisibleAddWord handleKeyDown={this._handleKeyDown.bind(this)}></VisibleAddWord>

                        </div>
                        <div className="col-md-6">
                            <Timer min={2} onTimeComplete={this._handleTimeUp.bind(this)}> </Timer>
                            <VisibleScoreBoard></VisibleScoreBoard>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    _wordInBoard(word){
        let board = this.props.store.getState().board;
        let bestMatches=[];
        let wordInBoard = false;

        // loop of characters in word
        let wordMatches = [];
        for (var i = 0; i < word.length; i++) {
            // loop of y dimension of board
            let new_char = true;
            let charInBoard = false;
            board.forEach((row,y)=>{
                // loop of x dimension of board
                row.forEach((cell,x) => {
                    if(word[i] === cell){
                        charInBoard = true;
                        if(i==0){
                            if(!new_char){
                                wordMatches.pop();
                                wordMatches.push({char: word[i], x: x, y:y });
                                bestMatches.push({words: wordMatches.slice(),
                                    validSquares: this.setValidSquares(wordMatches,x,y),
                                    matches: true});
                                new_char = false;
                            }
                            if(bestMatches.length ==0){
                                wordMatches.push({char: word[i], x: x, y:y });
                                bestMatches.push({words: wordMatches.slice(),
                                    validSquares: this.setValidSquares(wordMatches,x,y),
                                    matches: true});
                                new_char = false;
                            }
                        }
                        else{
                            bestMatches.forEach(match=>{
                                // check if belongs in neighboaring squares
                                if(!new_char){
                                    wordMatches.pop();
                                    wordMatches.push({char: word[i], x: x, y:y });
                                    bestMatches.push({words: wordMatches.slice(),
                                        validSquares: this.setValidSquares(wordMatches,x,y),
                                        matches: true});
                                    new_char = false;
                                }else
                                    if(match.validSquares.some(mat=>mat.x==x && mat.y==y))
                                    {
                                        wordMatches.push({char: word[i], x: x, y:y });
                                        match.words = [];
                                        match.words.push(...wordMatches);
                                        match.matches = true;
                                        match.validSquares = this.setValidSquares(match.words,x,y);
                                        new_char = false;
                                    }
                            });}

                    }
                });});
            bestMatches = this.getBestMatch(bestMatches);
            if(!charInBoard)
                wordInBoard =  false;
        }
        if(bestMatches.length >0)
            wordInBoard = true;
        else
            wordInBoard = false;
        return wordInBoard;
    }
    _handleKeyDown(e,v){
        if(e.key === 'Enter'){
            let word = e.target.value.toLowerCase();
            let error = '';
            if(!this._wordInBoard(word.toUpperCase()))
            {
                error = 'Word is not in board';
            }else if(this.props.store.getState().words.indexOf(word) > 0){
                let scoreboard = this.props.store.getState().scoreboard;
                if(!scoreboard.some(s=>s.word === word)){
                    this.props.store.dispatch(addWordToScoreBoard(word));
                }
                else
                    error = 'Word already played';
            }
            else 
                error = 'Invalid word';
            this.props.store.dispatch(setError(error));
            e.target.value = '';
        }
    }
    _handleTimeUp(){
        this.props.store.dispatch(stopGame());
        this.props.history.push('/results');
    }

    isRunning(){
        return  this.props.store.getState().running;
    }

    getBestMatch(matches) {
        var new_matches = matches.filter(m=> m.matches==true);
        new_matches.forEach(m=>m.matches = false);
        return new_matches;
    }
    setValidSquares(matches, x, y){
        let squares = [ {x: x+1, y:y+0}, {x: x+1, y:y+1}, {x: x+0, y:y+1},
            {x: x+1, y:y-1}, {x: x-1, y:y-0}, {x: x-1, y:y-1}, 
            {x: x-1, y:y+1}, {x: x-0, y:y-1}
        ]
        let returnSquares= [];
        squares.forEach(s=>{
            ///checking for visited squares. Not adding to validSquares
            if(!matches.some(m=> s.x==m.x && s.y==m.y))
                returnSquares.push(s);

        });
        return returnSquares;
    }
}

export default withRouter(Game)
