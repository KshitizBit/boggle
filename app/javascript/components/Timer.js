import React from "react"
import PropTypes from "prop-types"
import Countdown from "react-countdown"

class Timer extends React.Component {
    constructor(props) {
        super(props);
    }
  render () {
    return (
      <React.Fragment>
          <div className="row">
          <h6 className="display-5 col-md-2"> Timer </h6>
           <div className="col-md-4">
          <Countdown daysInHours = {true} date = {Date.now() + this.props.min*60*1000 }  onComplete={this.props.onTimeComplete}/>
           </div>
          </div>
      </React.Fragment>
    );
  }
}

export default Timer
