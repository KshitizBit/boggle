import React from "react"
import PropTypes from "prop-types"
class AddWord extends React.Component {
    constructor(props){
        super(props);
    }

  render () {
    return (
      <React.Fragment>
           <label className="lead">Add Word: </label>
           <span className="ml-1 text-danger">{this.props.error}</span>
           <input autoFocus type ='text' className="form-control" 
           disabled= {this.props.isRunning? "": "disabled"} onKeyDown ={this.props.handleKeyDown}/>
      </React.Fragment>
    );
  }
}

export default AddWord
