import React from "react"
import PropTypes from "prop-types"
import Square from "./Square"

class Board extends React.Component {
  constructor(props){
      super(props);
  }
  render () {
    return ( <div> { this.renderBoard() } </div>);
  }

 renderBoard(){
     return this.props.board.map((row,i)=>(
         <div key={i} className = "board-row">
             {row.map((s,j)=>this.renderSquare(s,j))}
         </div>))
 }
 renderSquare(char, index){
        return <Square key={index} value={index} char ={char}></Square>;
  }
}

export default Board
