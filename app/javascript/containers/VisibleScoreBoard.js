import {connect} from 'react-redux';
import ScoreBoard from '../components/ScoreBoard'

const mapStateToProps = state =>({
    scoreboard: state.scoreboard
})

export default connect(mapStateToProps)(ScoreBoard)
