import  {connect} from 'react-redux';
import AddWord from '../components/AddWord';

const mapStateToProps = state =>({
    isRunning: state.running,
    error: state.error
})

export default connect(mapStateToProps)(AddWord)
