import {applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import {FETCH_WORDS, SET_ERR, STOP_GAME, ADD_WORD_TO_SCOREBOARD } from './actionTypes';


const BOARDSIZE =4;

const initialState = {
    scoreboard: [],
    //board is a 2D array
    board: instantiateBoard(BOARDSIZE),
    running: true,
    error: '',
    words: []
};

function instantiateBoard(dimension){
    let gameboard = [];
    for (var i = 0 ; i < dimension; i++) {
      let row = [];
      for(var j =0; j< dimension; j++){
          row.push(assignLetter());
      }
        gameboard.push(row);
    }
   //  gameboard = [["J", "F", "E", "V"], ["X", "L", "O", "C"],["P", "L", "L", "B"],["T", "D", "S", "C"]];
   // gameboard = [["W", "Q", "W", "B"] ,["U", "T", "O", "R"] ,["W", "S", "E", "G"] ,["A", "I", "O", "Y"]];
   //  gameboard = [["Z", "E", "R", "Q"], ["R", "Q", "I", "M"], ["E", "E", "Q", "L"], ["V", "I", "O", "Q"]];
    return gameboard;
}
function assignLetter(){
   let characters= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   return characters.charAt(Math.floor(Math.random() * characters.length));
}

function rootReducer(state, action) {
    switch (action.type) {
        case ADD_WORD_TO_SCOREBOARD:
            return Object.assign({}, state, {
                scoreboard : [
                    ...state.scoreboard,
                    {
                        word: action.word,
                        score: action.score
                    }
                ]
            });
        case STOP_GAME:
            return Object.assign({}, state, {
                running : false
            });
        case SET_ERR:
            return Object.assign({}, state, {
                error : action.error
            });
        case FETCH_WORDS:
            return Object.assign({}, state, {
                words : action.words
            });
        default:
            return state;
    }
}

const middlewares = [thunk]
const store =  createStore(rootReducer, initialState, applyMiddleware(...middlewares));
export default store;
