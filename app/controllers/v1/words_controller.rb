class V1::WordsController < ApplicationController
    def get_words
        words = Rails.cache.fetch('words') do
            read_words()
         end
        render json: { :words => words}.to_json
    end
    
    def read_words
        words = [];
        File.foreach("#{Rails.root}/config/sowpods.txt") do |line|
            words.push(line.chomp)
        end
        return words
    end
end
